#include <stdio.h>

int get_start() {
    int num;
    while(1) {
        printf("Please enter a starting value for n: ");
        scanf("%d", &num);
        if(num > 0)
            break;
        printf("The number must be greater than zero!\n");
    }
    return num;
}

int next_collatz(int input) {
    if(input % 2 == 0) {
        return input / 2;
    }
    return input * 3 + 1;
}

int main() {
    int num = get_start();
    int length = 1;
    do {
        printf("%d, ", num);
        num = next_collatz(num);
        length++;
    } while(num != 1);
    printf("1\nLength: %d\n", length);
}