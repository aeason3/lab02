#include <stdio.h>

long int get_cc() {
    long int card_num;
    printf("Please enter a credit card number: ");
    scanf("%ld", &card_num);
    return card_num;
}

long int base_ten(long int exp) {
    long int i, result = 1;
    for (i = 0; i < exp; i++) {
        result *= 10;
    }
    return result;
}

char validate(long int card_num) {
    int accumulator = 0;
    int digit;
    for(int index = 1; index <= 16; index++) {
        digit = (card_num % base_ten(index)) / base_ten(index-1);
        accumulator += index % 2 == 0
            ? ((digit*2)/10)+((digit*2)%10)
            : digit;
    }
    
    if(accumulator % 10 != 0) {
        return 'i';
    } else if(digit == 3) {
        return 'a';
    } else if(digit == 4) {
        return 'v';
    } else if(digit == 5) {
        return 'm';
    } else if(digit == 6) {
        return 'd';
    }
    return 'i';
}

int main() {
    char result = validate(get_cc());
    switch(result) {
        case 'v':
            printf("VISA\n");
            break;
        case 'm':
            printf("MASTERCARD\n");
            break;
        case 'd':
            printf("DISCOVER\n");
            break;
        case 'a':
            printf("AMEX\n");
            break;
        case 'i':
            printf("INVALID\n");
            break;
    }
}